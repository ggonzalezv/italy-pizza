<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Factura
 * @package App\Models
 * @version June 2, 2021, 4:03 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $compras
 * @property string $total
 * @property integer $usuario_id
 * @property integer $local_id
 * @property string $observacion
 */
class Factura extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'factura';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'total',
        'usuario_id',
        'local_id',
        'observacion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'total' => 'string',
        'usuario_id' => 'integer',
        'local_id' => 'integer',
        'observacion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'total' => 'nullable|string|max:45',
        'usuario_id' => 'required|integer',
        'local_id' => 'required|integer',
        'observacion' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compras()
    {
        return $this->hasOne(\App\Models\Compra::class, 'factura_idfactura')->with(["localHasProducto"]);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function totalCompras()
    {
        return $this->hasMany(\App\Models\Compra::class, 'factura_idfactura')->with(["localHasProducto", "producto"]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function usuario()
    {
        return $this->belongsTo(\App\Models\Usuario::class, 'usuario_id')->with(["users"]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function local()
    {
        return $this->belongsTo(\App\Models\Local::class, 'local_id');
    }
}
