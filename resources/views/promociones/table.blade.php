<div class="table-responsive">
    <table class="table" id="promociones-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Producto/Local</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($promociones as $promociones)
            <tr>
                <td>{{ $promociones->nombre }}</td>
                <td>{{ $promociones->descripcion }}</td>
                <td>{{ $promociones->localHasProducto->nombre }}</td>
                <td width="120">
                    {!! Form::open(['route' => 'promociones.store']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('promociones.show', [$promociones->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('promociones.edit', [$promociones->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro de eliminar esta promoción?')"]) !!}
                        <a href="#" class='btn btn-default btn-xs' data-toggle="modal" data-target="#exampleModal" onclick="notificar('{{$promociones->id}}');">
                            <i class="far fa-envelope"></i>
                        </a>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Envio de promociones por correo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(['route' => 'notificar']) !!}
        <div class="modal-body">
          <input type="hidden" name="idPromocion" id="idPromocion">
          <textarea name="invitacion_promocion" id="invitacion_promocion" cols="30" rows="10" placeholder="Ingrese el texto para promocionar invitación" class="form-control"></textarea>
          <br />
          <label for="listado_usuarios">Seleccione los usuarios para notificar la promoción:</label>
          {!! Form::select('local_has_producto_id', $usuarios, null, ['class' => 'form-control', 'required' => true, 'multiple'=>'multiple', 'name'=>'correo_usuarios[]']) !!}
          <small style="opacity: .5;">Para seleccionar varios usuarios presione las teclas Ctrl + clic.</small>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Notificar promoción</button>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script>
// función que obtiene el envento click para enviar las notificaciones via correo electrónico
function notificar(e) {
  // Se asigna el valor de la promoción recibida por parámetro, para posteriormente enviar al controlador
  $('#idPromocion').val(e);
}

</script>