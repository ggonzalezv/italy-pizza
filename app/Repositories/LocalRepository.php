<?php

namespace App\Repositories;

use App\Models\Local;
use App\Repositories\BaseRepository;

/**
 * Class LocalRepository
 * @package App\Repositories
 * @version May 3, 2021, 7:24 pm UTC
*/

class LocalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'direccion',
        'celular'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Local::class;
    }
}
