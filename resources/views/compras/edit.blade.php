@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Editar Compra</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {{-- {!! Form::model($compra, ['route' => ['compras.update', $compra->id], 'method' => 'patch']) !!} --}}
            <form @submit.prevent="update({{$compra->id}})">

            <div class="card-body">

                <div class="row">
                    @include('compras.fields')
                </div>
                <dynamic-list 
                        label-button-add="Agregar ítem a la lista" 
                        :data-list.sync="dataForm.pedido" 
                        class-table="table-responsive table-bordered" 
                        class-container="w-100 p-10"
                        :data-list-options="[
                                    {label:'Cantidad', name:'cantidad', isShow: true},
                                    {label:'Producto', name:'producto_id', isShow: true, refList: 'selectRefAsset'}
                                    ]">
                        <template #fields="scope">

                            <div class="form-group row m-b-15">
                                <!-- Accessory Parts Field -->
                                <label class="col-form-label col-md-2 required" for="accessory_parts" >Cantidad:</label>
                                <div class="col-md-4">
                                    <input type="number" id="cantidad" :class="{'form-control':true, 'is-invalid':dataErrors.cantidad }" v-model="scope.dataForm.cantidad" required>
                                    <small>Ingrese la cantidad del producto</small>
                                </div>
                                <!-- Amount Field -->
                                <label class="col-form-label col-md-2 required" for="producto" >Producto:</label>
                                <div class="col-md-4">
                                    <select-check ref-select-check="selectRefAsset" css-class="form-control" name-field="producto_id" reduce-label="nombre" reduce-key="id" name-resource="/get-productos-precio" :value="scope.dataForm" :is-required="true"></select-check>
                                    <small>Ingrese el producto</small>
                                </div>
                            </div>

                        </template>
                </dynamic-list>

                <div style="font-size: 22px; font-weight: bold; color: green;">Total Compra: $@{{ formatPrice(dataForm.pedido_total) }}</div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('compras.index') }}" class="btn btn-default">Cancelar</a>
            </div>

            <!-- {!! Form::close() !!} -->
            </form>

        </div>
    </div>
@endsection
