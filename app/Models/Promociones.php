<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Promociones
 * @package App\Models
 * @version May 6, 2021, 3:31 pm UTC
 *
 * @property \App\Models\LocalHasProducto $localHasProducto
 * @property string $nombre
 * @property string $descripcion
 * @property integer $local_has_producto_id
 */
class Promociones extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'promociones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'descripcion',
        'local_has_producto_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'descripcion' => 'string',
        'local_has_producto_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'nullable|string|max:45',
        'descripcion' => 'nullable|string|max:45',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable',
        'local_has_producto_id' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function localHasProducto()
    {
        return $this->belongsTo(\App\Models\LocalHasProducto::class, 'local_has_producto_id')->with(['producto', 'local']);
    }
}
