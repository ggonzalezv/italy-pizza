<!-- Local Has Producto Id Field -->
<div class="form-group row col-md-12">
    {!! Form::label('local_has_producto_id', 'Local:', ['class' => 'col-form-label col-md-2']) !!}
    <div class="col-md-4">
    {!! Form::select('local_has_producto_id', $locales, null, ['class' => 'form-control', 'v-model' => 'dataForm.local_has_producto_id', 'required' => true]) !!}
    <small>Seleccione el local de la compra</small>
    </div>

    <!-- Usuario Id Field -->
    {!! Form::label('usuario_id', 'Usuario:', ['class' => 'col-form-label col-md-2']) !!}
    <div class="col-md-4">
    {!! Form::select('usuario_id', $usuarios, null, ['class' => 'form-control', 'v-model' => 'dataForm.usuario_id']) !!}
    <small>Seleccione el usuario que realiza la compra</small>
    </div>
</div>

<hr style="width: 100%;" />