<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $producto->nombre }}</p>
</div>

<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $producto->descripcion }}</p>
</div>

<!-- Puntos Field -->
<div class="col-sm-12">
    {!! Form::label('puntos', 'Puntos:') !!}
    <p>{{ $producto->puntos }}</p>
</div>

<!-- Imagen Principal Field -->
<div class="col-sm-12">
    {!! Form::label('imagen_principal', 'Imagen Principal:') !!}
    <p><a target="_blank" href="{{ url('/storage/'.$producto->imagen_principal) }}">Ver adjunto</a></p>
</div>

