<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45]) !!}
</div>

<!-- Local Has Producto Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('local_has_producto_id', 'Producto/Local:') !!}
    {!! Form::select('local_has_producto_id', $promociones['producto'], null, ['class' => 'form-control', 'required' => true]) !!}
</div>