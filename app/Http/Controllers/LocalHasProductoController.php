<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLocalHasProductoRequest;
use App\Http\Requests\UpdateLocalHasProductoRequest;
use App\Repositories\LocalHasProductoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class LocalHasProductoController extends AppBaseController
{
    /** @var  LocalHasProductoRepository */
    private $localHasProductoRepository;

    public function __construct(LocalHasProductoRepository $localHasProductoRepo)
    {
        $this->localHasProductoRepository = $localHasProductoRepo;
    }

    /**
     * Display a listing of the LocalHasProducto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $localHasProductos = $this->localHasProductoRepository->all();

        return view('local_has_productos.index')
            ->with('localHasProductos', $localHasProductos);
    }

    /**
     * Show the form for creating a new LocalHasProducto.
     *
     * @return Response
     */
    public function create()
    {
        return view('local_has_productos.create');
    }

    /**
     * Store a newly created LocalHasProducto in storage.
     *
     * @param CreateLocalHasProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateLocalHasProductoRequest $request)
    {
        $input = $request->all();

        $localHasProducto = $this->localHasProductoRepository->create($input);

        Flash::success('Local Has Producto guardado correctamente.');

        return redirect(route('localHasProductos.index'));
    }

    /**
     * Display the specified LocalHasProducto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $localHasProducto = $this->localHasProductoRepository->find($id);

        if (empty($localHasProducto)) {
            Flash::error('Local Has Producto no encontrado');

            return redirect(route('localHasProductos.index'));
        }

        return view('local_has_productos.show')->with('localHasProducto', $localHasProducto);
    }

    /**
     * Show the form for editing the specified LocalHasProducto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $localHasProducto = $this->localHasProductoRepository->find($id);

        if (empty($localHasProducto)) {
            Flash::error('Local Has Producto no encontrado');

            return redirect(route('localHasProductos.index'));
        }

        return view('local_has_productos.edit')->with('localHasProducto', $localHasProducto);
    }

    /**
     * Update the specified LocalHasProducto in storage.
     *
     * @param int $id
     * @param UpdateLocalHasProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLocalHasProductoRequest $request)
    {
        $localHasProducto = $this->localHasProductoRepository->find($id);

        if (empty($localHasProducto)) {
            Flash::error('Local Has Producto no encontrado');

            return redirect(route('localHasProductos.index'));
        }

        $localHasProducto = $this->localHasProductoRepository->update($request->all(), $id);

        Flash::success('Local Has Producto actualizado correctamente.');

        return redirect(route('localHasProductos.index'));
    }

    /**
     * Remove the specified LocalHasProducto from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $localHasProducto = $this->localHasProductoRepository->find($id);

        if (empty($localHasProducto)) {
            Flash::error('Local Has Producto no encontrado');

            return redirect(route('localHasProductos.index'));
        }

        $this->localHasProductoRepository->delete($id);

        Flash::success('Local Has Producto eliminado correctamente.');

        return redirect(route('localHasProductos.index'));
    }
}
