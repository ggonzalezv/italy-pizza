@servers(['web' => 'root@sevendemo.com -p 22022'])

@task('list', ['on' => 'web'])
    ls -l
@endtask

@setup
    $repository = 'https://gitlab.com/ggonzalezv/italy-pizza.git';
    $app_dir = '/home/sevendemo/italypizza.sevendemo.com/italy-pizza';
    $folder_web = '/home/sevendemo/italypizza.sevendemo.com';
    $release = date('YmdHis');
@endsetup

@story('deploy')
    clone_repository
    run_composer
    run_node
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    rm -rf {{ $app_dir }}
    git clone --depth 1 {{ $repository }} {{ $app_dir }}
    cp {{ $folder_web }}/.env {{ $app_dir }}
    chown -R sevendemo.sevendemo {{ $app_dir }}
    cd {{ $app_dir }}
    git reset --hard {{ $commit }} 
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $app_dir }}
    composer install --prefer-dist --no-scripts -q -o
    php artisan storage:link
@endtask

@task('run_node')
    cd {{ $app_dir }}
    npm install
    npm run dev
@endtask