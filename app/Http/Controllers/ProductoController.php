<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductoRequest;
use App\Http\Requests\UpdateProductoRequest;
use App\Repositories\ProductoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Producto;
use App\Models\Local;
use App\Models\LocalHasProducto;
use Illuminate\Database\Eloquent\Builder;

class ProductoController extends AppBaseController
{
    /** @var  ProductoRepository */
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepo)
    {
        $this->productoRepository = $productoRepo;
    }

    /**
     * Obtiene todos los elementos existentes
     *
     * @author Jhoan Sebastian Chilito S. - Abr. 06 - 2020
     * @version 1.0.0
     *
     * @return Response
     */
    public function all(Request $request) {
        $asset_types = $this->productoRepository->all();
        // $asset_types = AssetType::with(['dependency'])->latest()->get();
        return $this->sendResponse($asset_types->toArray(), trans('data_obtained_successfully'));
    }

    public function getProductos(Request $request) {


        $productos = Producto::with(["precio" => function ($query) {
            $query->select('producto_id', 'precio');
        }])->latest()->get();
        // $productos = Producto::with(["precio" => function(Builder $query) {
        //     $query->where("local_id", 1);
        // }])->get();

        // dd($productos->toArray());
        // $asset_types = $this->productoRepository->all();
        // // $asset_types = AssetType::with(['dependency'])->latest()->get();
        return $this->sendResponse($productos->toArray(), trans('data_obtained_successfully'));
    }

    /**
     * Display a listing of the Producto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productos = Producto::latest()->get();

        return view('productos.index')
            ->with('productos', $productos);
    }

    /**
     * Show the form for creating a new Producto.
     *
     * @return Response
     */
    public function create()
    {
        $locales = Local::all()->pluck('nombre', 'id');
        return view('productos.create')->with('locales', $locales);
    }

    /**
     * Store a newly created Producto in storage.
     *
     * @param CreateProductoRequest $request
     *
     * @return Response
     */
    public function store(CreateProductoRequest $request)
    {
        $input = $request->all();

        // Valida si se ingresa la imagen de perfil
        if ($request->hasFile('imagen_principal')) {
            // dd("entro");
            $input['imagen_principal'] = substr($input['imagen_principal']->store('public/imagenes_producto'), 7);
        }

        $producto = $this->productoRepository->create($input);

        LocalHasProducto::create([
            'precio' => $input['precio'],
            'local_id' => $input['local_id'],
            'producto_id' => $producto->id
        ]);

        Flash::success('Producto guardado correctamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Display the specified Producto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $producto = $this->productoRepository->find($id);

        if (empty($producto)) {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        return view('productos.show')->with('producto', $producto);
    }

    /**
     * Show the form for editing the specified Producto.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $producto = $this->productoRepository->find($id);
        $locales = Local::all()->pluck('nombre', 'id');
        
        if (empty($producto)) {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        return view('productos.edit')->with('producto', $producto)->with('locales', $locales);
    }

    /**
     * Update the specified Producto in storage.
     *
     * @param int $id
     * @param UpdateProductoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductoRequest $request)
    {
        $producto = $this->productoRepository->find($id);

        $input = $request->all();

        if (empty($producto)) {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }
        // Valida si se va a actualizar la imagen principal del producto
        if ($request->hasFile('imagen_principal')) {
            $input['imagen_principal'] = substr($input['imagen_principal']->store('public/imagenes_producto'), 7);
        } else {
            // Si no va actualizar la imagen del producto, elimina la variable "imagen_principal" del array para que no actualice la ruta
            unset($input['imagen_principal']);
        }

        $producto = $this->productoRepository->update($input, $id);

        Flash::success('Producto actualizado correctamente.');

        return redirect(route('productos.index'));
    }

    /**
     * Remove the specified Producto from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $producto = $this->productoRepository->find($id);

        if (empty($producto)) {
            Flash::error('Producto no encontrado');

            return redirect(route('productos.index'));
        }

        $this->productoRepository->delete($id);

        Flash::success('Producto eliminado correctamente.');

        return redirect(route('productos.index'));
    }
}
