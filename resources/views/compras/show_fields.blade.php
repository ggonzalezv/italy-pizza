<!-- created_at Field -->
<div class="col-sm-6">
    {!! Form::label('created_at', 'Fecha:') !!}
    <p>{{ $compra["created_at"] }}</p>
</div>

<!-- Usuario Id Field -->
<div class="col-sm-6">
    {!! Form::label('usuario_id', 'Usuario:') !!}
    <p>{{ $compra["usuario"]["users"]["name"] }}</p>
</div>

<!-- Local Field -->
<div class="col-sm-12">
    {!! Form::label('local_id', 'Local:') !!}
    <p>{{ $compra["local"]["nombre"] }}</p>
</div>

<hr style="width: 100%;" />

<div class="table-responsive">
    <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th>Cantidad</th>
                <th>Producto</th>
                <th>Observación</th>
            </tr>
        </thead>
        <tbody>
            @foreach($compra["total_compras"] as $comp)
                <tr>
                    <td>{{$comp["cantidad"]}}</td>
                    <td>{{$comp["producto"]["nombre"]}}</td>
                    <td>{{$comp["observacion"]}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div style="font-size: 22px; font-weight: bold; color: green;">Total Compra: $@{{ formatPrice({!! $compra["total"] !!}) }}</div>

