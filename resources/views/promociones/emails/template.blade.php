<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style>

		.container{
		}

		table{
			border: 1px #dceaf5 solid;
			border-collapse: collapse;
			border-radius: 4px;
			width: 100%;
		}

		table td{
			border: 1px #dceaf5 solid;
			padding: 10px;
		}

		.avisoLegal{
			font-style: italic;
			text-align: justify;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<p style="font-size:16px;text-align:justify"><b>Por favor <span>no responda</span> a este correo electrónico ya que el <?php echo config('mail.username') ?> solo funciona para notificaciones y no para trámites.</b></p>
		</div>
		<div class="row">
			<table>
				<tr style="background-color: #8e2a2a; color: white;">
					<td><img style="width: 120px;float: left;padding-right: 20px;" src="imagenes_sistema/logo.jpeg"><h3> Italy Pizza </h3></td>
				</tr>
				<tr>
					<td colspan="100%">
                        <p>
                            <p>Un saludo cordial usuario <br><br>

                                Te invitamos a pedir esta superpromoción ubicada en <strong>{{ $promocion['localHasProducto']['local']['nombre'] }}</strong>
                                <br><br>

                                <strong>{{ $promocion['localHasProducto']['producto']['nombre'] }}</strong>
                                <br />
                                <img src="{{ $promocion['localHasProducto']['producto']['imagen_principal'] }}" alt="Imagen promoción">


                                <h2>{{ $promocion['invitacion_promocion'] }}</h2>
                            </p>
                            <br />
                            <p>Para conocer mas detalles por favor acceda al sitio principal: <a href="http://localhost:8000/">Ir a Italy Pizza</a></p>
                        </p>
                    </td>
				</tr>
			
			</table>
		</div>
	</div>
</body>
</html>