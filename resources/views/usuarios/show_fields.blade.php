<!-- Cedula Field -->
<div class="col-sm-12">
    {!! Form::label('cedula', 'Cedula:') !!}
    <p>{{ $usuario->cedula }}</p>
</div>

<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Nombre:') !!}
    <p>{{ $usuario->users->name }}</p>
</div>

<!-- Correo electrónico Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Correo electrónico:') !!}
    <p>{{ $usuario->users->email }}</p>
</div>

<!-- Celular Field -->
<div class="col-sm-12">
    {!! Form::label('celular', 'Celular:') !!}
    <p>{{ $usuario->users->celular }}</p>
</div>

<!-- Fecha de nacimiento Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
    <p>{{ $usuario->users->fecha_nacimiento }}</p>
</div>

<!-- Ciudad Field -->
<div class="col-sm-12">
    {!! Form::label('ciudad', 'Ciudad:') !!}
    <p>{{ $usuario->users->ciudad->nombre }}</p>
</div>

<!-- Fecha de creación Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Fecha de creación:') !!}
    <p>{{ $usuario->users->created_at }}</p>
</div>

<!-- Fecha de actualización Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Fecha de actualización:') !!}
    <p>{{ $usuario->users->updated_at }}</p>
</div>