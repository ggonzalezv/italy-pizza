<li class="nav-item">
    <a href="{{ route('inicio') }}"
       class="nav-link">
        <i class="fas fa-home"></i>
        <p>Inicio</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('productos.index') }}"
       class="nav-link {{ Request::is('productos*') || Request::is('home*') ? 'active' : '' }}">
        <i class="fas fa-th-large"></i>
        <p>Productos</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('promociones.index') }}"
       class="nav-link {{ Request::is('promociones*') ? 'active' : '' }}">
        <i class="fas fa-poll"></i>
        <p>Promociones</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('usuarios.index') }}"
       class="nav-link {{ Request::is('usuarios*') ? 'active' : '' }}">
        <i class="fas fa-users"></i>
        <p>Usuarios</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('compras.index') }}"
       class="nav-link {{ Request::is('compras*') ? 'active' : '' }}">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <p>Compras</p>
    </a>
</li>


