<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class LocalHasProducto
 * @package App\Models
 * @version May 6, 2021, 3:30 pm UTC
 *
 * @property \App\Models\Local $local
 * @property \App\Models\Producto $producto
 * @property \Illuminate\Database\Eloquent\Collection $compras
 * @property \Illuminate\Database\Eloquent\Collection $promociones
 * @property string $precio
 * @property integer $local_id
 * @property integer $producto_id
 */
class LocalHasProducto extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'local_has_producto';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'precio',
        'local_id',
        'producto_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'precio' => 'string',
        'local_id' => 'integer',
        'producto_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'precio' => 'nullable|string|max:120',
        'local_id' => 'required|integer',
        'producto_id' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function local()
    {
        return $this->belongsTo(\App\Models\Local::class, 'local_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function producto()
    {
        return $this->belongsTo(\App\Models\Producto::class, 'producto_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function compras()
    {
        return $this->hasMany(\App\Models\Compra::class, 'local_has_producto_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function promociones()
    {
        return $this->hasMany(\App\Models\Promocione::class, 'local_has_producto_id');
    }
}
