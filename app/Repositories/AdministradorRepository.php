<?php

namespace App\Repositories;

use App\Models\Administrador;
use App\Repositories\BaseRepository;

/**
 * Class AdministradorRepository
 * @package App\Repositories
 * @version May 3, 2021, 12:38 pm UTC
*/

class AdministradorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'users_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Administrador::class;
    }
}
