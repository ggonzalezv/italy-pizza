<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Compra
 * @package App\Models
 * @version May 25, 2021, 12:39 am UTC
 *
 * @property \App\Models\Factura $facturafactura
 * @property \App\Models\LocalHasProducto $localHasProducto
 * @property \App\Models\Producto $producto
 * @property \App\Models\Usuario $usuario
 * @property string $observacion
 * @property integer $cantidad
 * @property integer $factura_idfactura
 * @property integer $producto_id
 * @property integer $local_has_producto_id
 */
class Compra extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'compras';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'observacion',
        'cantidad',
        'factura_idfactura',
        'producto_id',
        'local_has_producto_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'observacion' => 'string',
        'cantidad' => 'integer',
        'factura_idfactura' => 'integer',
        'producto_id' => 'integer',
        'local_has_producto_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'observacion' => 'nullable',
        'cantidad' => 'nullable|integer',
        'local_has_producto_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function facturafactura()
    {
        return $this->belongsTo(\App\Models\Factura::class, 'factura_idfactura');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function localHasProducto()
    {
        return $this->belongsTo(\App\Models\LocalHasProducto::class, 'local_has_producto_id')->with(["local", "producto"]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function producto()
    {
        return $this->belongsTo(\App\Models\Producto::class, 'producto_id');
    }
}
