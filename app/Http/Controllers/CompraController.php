<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCompraRequest;
use App\Http\Requests\UpdateCompraRequest;
use App\Repositories\CompraRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

Use App\Models\Producto;
Use App\Models\Local;
Use App\Models\Usuario;
Use App\Models\LocalHasProducto;
Use App\Models\Factura;

use Illuminate\Support\Facades\Session;
use SebastianBergmann\FileIterator\Facade;

class CompraController extends AppBaseController
{
    /** @var  CompraRepository */
    private $compraRepository;

    public function __construct(CompraRepository $compraRepo)
    {
        $this->compraRepository = $compraRepo;
    }

    /**
     * Display a listing of the Compra.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $factura = Factura::with(["compras"])->latest()->get();

        return view('compras.index')
            ->with('compras', $factura);
    }

    /**
     * Show the form for creating a new Compra.
     *
     * @return Response
     */
    public function create()
    {
        $productos = Producto::all()->pluck('nombre', 'id');
        $locales = Local::all()->pluck('nombre', 'id');
        $usuarios = Usuario::with(['users'])->get()->pluck('users.name', 'id');

        return view('compras.create')
                    ->with('productos', $productos)
                    ->with('locales', $locales)
                    ->with('usuarios', $usuarios);
    }

    /**
     * Store a newly created Compra in storage.
     *
     * @param CreateCompraRequest $request
     *
     * @return Response
     */
    public function store(CreateCompraRequest $request)
    {
        $input = $request->all();
        $input["local_id"] = $input["local_has_producto_id"];
        $input["total"] = $input["pedido_total"];
        $factura = Factura::create($input);

        foreach($input["pedido"] as $p) {
            $p["local_has_producto_id"] = $input["local_has_producto_id"];
            $p["observacion"] = $input["observacion"];
            $p["factura_idfactura"] = $factura->id;;

            $compra = $this->compraRepository->create($p);
        }

        Flash::success('Compra guardada correctamente.');

        return $this->sendResponse($compra, 'OK');
    }

    /**
     * Display the specified Compra.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $factura = Factura::with(["usuario", "local", "totalCompras"])->find($id);

        if (empty($factura)) {
            Flash::error('Compra no encontrado');

            return redirect(route('compras.index'));
        }

        $facturaArray = $factura->toArray();
        $facturaArray["created_at"] = date("Y-m-d h:i:s", strtotime($facturaArray["created_at"]));

        return view('compras.show')->with('compra', $facturaArray);
    }

    /**
     * Show the form for editing the specified Compra.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productos = Producto::all()->pluck('nombre', 'id');
        $locales = Local::all()->pluck('nombre', 'id');
        $usuarios = Usuario::with(['users'])->get()->pluck('users.name', 'id');

        $compra = $this->compraRepository->find($id);

        if (empty($compra)) {
            Flash::error('Compra no encontrada');

            return redirect(route('compras.index'));
        }

        return view('compras.edit')
                    ->with('compra', $compra)
                    ->with('productos', $productos)
                    ->with('locales', $locales)
                    ->with('usuarios', $usuarios);
    }

    /**
     * Update the specified Compra in storage.
     *
     * @param int $id
     * @param UpdateCompraRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompraRequest $request)
    {
        $compra = $this->compraRepository->find($id);

        if (empty($compra)) {
            Flash::error('Compra no encontrado');

            return redirect(route('compras.index'));
        }

        $compra = $this->compraRepository->update($request->all(), $id);

        Flash::success('Compra actualizada correctamente.');

        return redirect(route('compras.index'));
    }

    /**
     * Remove the specified Compra from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $compra = Factura::find($id);

        if (empty($compra)) {
            Flash::error('Compra no encontrada');

            return redirect(route('compras.index'));
        }

        $compra->delete($id);

        Flash::success('Compra eliminada correctamente.');

        return redirect(route('compras.index'));
    }
}
