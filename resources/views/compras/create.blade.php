@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Crear Compra</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {{-- {!! Form::open(['route' => 'compras.store']) !!} --}}
            <form @submit.prevent="submit">

            <div class="card-body">

                <div class="row">
                    @include('compras.fields')
                </div>
                <dynamic-list 
                        label-button-add="Agregar compra a la lista" 
                        :data-list.sync="dataForm.pedido" 
                        class-table="table-responsive table-bordered" 
                        class-container="w-100 p-10"
                        :data-list-options="[
                                    {label:'Cantidad', name:'cantidad', isShow: true},
                                    {label:'Producto', name:'producto_id', isShow: true, refList: 'selectRefAsset'},
                                    {label:'Observación', name:'observacion', isShow: true}
                                    ]">
                        <template #fields="scope">

                            <div class="form-group row m-b-15">
                                <!-- Accessory Parts Field -->
                                <label class="col-form-label col-md-2 required" for="accessory_parts" >Cantidad:</label>
                                <div class="col-md-4">
                                    <input type="number" id="cantidad" :class="{'form-control':true, 'is-invalid':dataErrors.cantidad }" v-model="scope.dataForm.cantidad" required>
                                    <small>Ingrese la cantidad del producto a comprar</small>
                                </div>
                                <!-- Amount Field -->
                                <label class="col-form-label col-md-2 required" for="producto" >Producto:</label>
                                <div class="col-md-4">
                                    <select-check ref-select-check="selectRefAsset" css-class="form-control" name-field="producto_id" reduce-label="nombre" reduce-key="id" name-resource="/get-productos-precio" :value="scope.dataForm" :is-required="true"></select-check>
                                    <small>Ingrese el producto</small>
                                </div>
                            </div>
                            <div class="form-group row m-b-15">
                                <!-- Observacion Field -->
                                <label class="col-form-label col-md-2" for="observacion">Observación:</label>
                                <div class="col-md-4">
                                    <textarea id="observacion" :class="{'form-control':true, 'is-invalid':dataErrors.observacion }" v-model="scope.dataForm.observacion"></textarea>
                                    <small>Ingrese alguna observación si es necesario</small>
                                </div>
                            </div>

                        </template>
                </dynamic-list>
                <div style="font-size: 22px; font-weight: bold; color: green;">Total Compra: $@{{ formatPrice(dataForm.pedido_total) }}</div>

                <hr style="width: 100%;" />

                <!-- Observacion Field -->
                <div class="form-group row m-b-15">
                    {!! Form::label('observacion', 'Observación:', ['class' => 'col-form-label col-md-3']) !!}
                    <div class="col-md-9">
                        {!! Form::textarea('observacion', null, ['class' => 'form-control', 'v-model' => 'dataForm.observacion']) !!}
                        <small>Ingrese una observación.</small>
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
                <a href="{{ route('compras.index') }}" class="btn btn-default"><i class="fa fa-times"></i> Cancelar</a>
            </div>

            {{-- {!! Form::close() !!} --}}
            </form>

        </div>
    </div>
@endsection
