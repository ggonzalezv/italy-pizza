<?php

namespace App\Repositories;

use App\Models\LocalHasProducto;
use App\Repositories\BaseRepository;

/**
 * Class LocalHasProductoRepository
 * @package App\Repositories
 * @version May 6, 2021, 3:30 pm UTC
*/

class LocalHasProductoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'precio',
        'local_id',
        'producto_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LocalHasProducto::class;
    }
}
