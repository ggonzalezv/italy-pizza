<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Pavo is a mobile app Bootstrap HTML template created to help you present benefits, features and information about mobile apps in order to convince visitors to download them">
    <meta name="author" content="Your name">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on Facebook, Twitter, LinkedIn -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta name="twitter:card" content="summary_large_image"> <!-- to have large image post format in Twitter -->

    <!-- Webpage Title -->
    <title>Italy Pizza</title>
    
    <!-- Styles -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">
    <link href="cssLanding/bootstrap.css" rel="stylesheet">
    <link href="cssLanding/fontawesome-all.css" rel="stylesheet">
    <link href="cssLanding/swiper.css" rel="stylesheet">
	<link href="cssLanding/magnific-popup.css" rel="stylesheet">
	<link href="cssLanding/styles.css" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="imagesLanding/favicon.png">
</head>
<body data-spy="scroll" data-target=".fixed-top" src="imagenes_sistema/logo.jpeg">
    
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <div class="container">
            
            <!-- Text Logo - Use this if you don't have a graphic logo -->
            <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Pavo</a> -->

            <!-- Image Logo -->
            <!-- <div class="navbar-brand logo-image">Italy Pizza</div>  -->
            <a class="navbar-brand logo-image" href="index.html"><img src="imagenes_sistema/logo.jpeg" alt="alternative" style="width: auto; height: 54px;"></a> 

            <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#header">Inicio Uniquindio 2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#features">Productos</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#quienes_somos">Quienes Somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="{{ route('login') }}">Ingresar como usuario</a>
                    </li>
                </ul>
            </div> <!-- end of navbar-collapse -->
        </div> <!-- end of container -->
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h1 class="h1-large">Italy Pizza</h1>
                        <h3>Distinganos por el sabor...</h3>
                        <p class="p-large">Ordene la pizza a su  gusto</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="imagenes_sistema/logo.jpeg" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </header> <!-- end of header -->
    <!-- end of header -->


    <!-- Introduction -->
    <div class="basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>Nuestros productos</p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of introduction -->

    <!-- Features -->
    <div id="features" class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    
                    @foreach($productos as $producto)
                        <!-- Card -->
                        <div class="card">
                            <div class="card-image">
                                <img class="img-fluid" src="{{ '/storage/'.$producto->imagen_principal }}" alt="alternative">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{ $producto->nombre }}</h5>
                                <p>{{ $producto->descripcion }}</p>
                            </div>
                        </div>
                        <!-- end of card -->
                    @endforeach


                    <!-- Card -->
                    <!-- <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="imagesLanding/features-icon-2.svg" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Easy On Resources</h5>
                            <p>Works smoothly even on older generation hardware due to our optimization efforts</p>
                        </div>
                    </div> -->
                    <!-- end of card -->

                    <!-- Card -->
                    <!-- <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="imagesLanding/features-icon-3.svg" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Great Performance</h5>
                            <p>Optimized code and innovative technology insure no delays and ultra-fast responsiveness</p>
                        </div>
                    </div> -->
                    <!-- end of card -->

                    <!-- Card -->
                    <!-- <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="imagesLanding/features-icon-4.svg" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Multiple Languages</h5>
                            <p>Choose from one of the 40 languages that come pre-installed and start selling smarter</p>
                        </div>
                    </div> -->
                    <!-- end of card -->

                    <!-- Card -->
                    <!-- <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="imagesLanding/features-icon-5.svg" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Free Updates</h5>
                            <p>Don't worry about future costs, pay once and receive all future updates at no extra cost</p>
                        </div>
                    </div> -->
                    <!-- end of card -->

                    <!-- Card -->
                    <!-- <div class="card">
                        <div class="card-image">
                            <img class="img-fluid" src="imagesLanding/features-icon-6.svg" alt="alternative">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Community Support</h5>
                            <p>Register the app and get acces to knowledge and ideas from the Pavo online community</p>
                        </div>
                    </div> -->
                    <!-- end of card -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of features -->

    <!-- Introduction -->
    <div class="basic-1" id="quienes_somos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>¡Quiénes Somos!</p>
                    <p style="text-align: justify;">
                        Somos una Pizzeria ubicada en el municipio de Circacia y Barcelona que apuesta por ofrecer pizzas con una masa crujiente y de gran sabor, pasta fresca con salsas caseras elaboradas con gran pasión y recetas mejoradas con el paso de los años. Nuestra principal filosofía, conseguir que cada uno de los comensales que visitan nuestra Pizzeria queden satisfechos con el servicio recibido y la calidad de todas las elaboraciones. Por ello, trabajamos con materia prima de calidad y buscamos en el mercado los productos más frescos.
                        <br /><br />
                        A través de la experiencia con nuestros clientes hemos ido mejorando tanto la calidad de nuestros productos y la eficiencia de nuestros servicios, gracias al constante contacto y vinculación de quienes nos prefieren.
                    </p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of introduction -->

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4>Italy Pizza es un sitio de comida rápida, ubicados en dos sedes, en el municio de Circasia y en el municio de Barcelona. <a class="purple" href="mailto:email@domain.com">contactoItalyPizza@gmail.com</a></h4>
                    <div class="social-container">
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-pinterest-p fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-youtube fa-stack-1x"></i>
                            </a>
                        </span>
                    </div> <!-- end of social-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->  
    <!-- end of footer -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul class="list-unstyled li-space-lg p-small">
                        <li><a href="terms.html">Terminos & Condiciones</a></li>
                    </ul>
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <p class="p-small statement">Copyright © <a href="#your-link">Italy Pizza</a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    	
    <!-- Scripts -->
    <script src="jsLanding/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="jsLanding/bootstrap.min.js"></script> <!-- Bootstrap framework -->
    <script src="jsLanding/jquery.easing.min.js"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="jsLanding/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="jsLanding/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
    <script src="jsLanding/scripts.js"></script> <!-- Custom scripts -->
</body>
</html>