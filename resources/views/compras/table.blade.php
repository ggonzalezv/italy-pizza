<div class="table-responsive">
    <table class="table" id="compras-table">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Factura No.</th>
                <th>Total Compra</th>
                <th>Local</th>
                <th>Usuario</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($compras as $compra)
            <tr>
                <td>{{ $compra->created_at }}</td>
                <td>{{ $compra->id }}</td>
                <td>$@{{ formatPrice({!! $compra->total !!}) }}</td>
                <td>{{ $compra->compras->localHasProducto->local->nombre }}</td>
                <td>{{ $compra->usuario->users->name }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['compras.destroy', $compra->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('compras.show', [$compra->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        {{-- <a href="{{ route('compras.edit', [$compra->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a> --}}
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
