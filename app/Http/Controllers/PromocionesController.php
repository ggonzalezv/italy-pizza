<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePromocionesRequest;
use App\Http\Requests\UpdatePromocionesRequest;
use App\Repositories\PromocionesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

use App\Models\Promociones;
use App\Models\LocalHasProducto;
use App\Models\Usuario;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class PromocionesController extends AppBaseController
{
    /** @var  PromocionesRepository */
    private $promocionesRepository;

    public function __construct(PromocionesRepository $promocionesRepo)
    {
        $this->promocionesRepository = $promocionesRepo;
    }

    /**
     * Display a listing of the Promociones.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $promociones = Promociones::with(['localHasProducto' => function($q) {
            $q->select(DB::raw('CONCAT(producto.nombre, "/", local.nombre) AS nombre'), 'local_has_producto.id')
            ->join('producto', 'producto.id', 'local_has_producto.producto_id')
            ->join('local', 'local.id', 'local_has_producto.local_id');
        }])->get();

        // dd($promociones);

        $usuarios = Usuario::with(['users' => function ($q) {
            $q->selectRaw('CONCAT(name," - ",email) AS nombre, email, id');
        }])->get()->pluck('users.nombre', 'users.email');

        return view('promociones.index')
            ->with('promociones', $promociones)
            ->with('usuarios', $usuarios); // Se usa para el envío de notificaciones
    }

    /**
     * Show the form for creating a new Promociones.
     *
     * @return Response
     */
    public function create()
    {
        $LocalHasProducto['producto'] = LocalHasProducto::select(DB::raw('CONCAT(producto.nombre, "/", local.nombre) AS nombre'), 'local_has_producto.id')
                        ->join('producto', 'producto.id', 'local_has_producto.producto_id')
                        ->join('local', 'local.id', 'local_has_producto.local_id')
                        ->get()
                        ->pluck('nombre', 'id');

        // $LocalHasProducto['producto'] = $LocalHasProducto;

        // dd($LocalHasProducto);
        return view('promociones.create')->with('promociones', $LocalHasProducto);
    }

    /**
     * Store a newly created Promociones in storage.
     *
     * @param CreatePromocionesRequest $request
     *
     * @return Response
     */
    public function store(CreatePromocionesRequest $request)
    {
        $input = $request->all();

        $promociones = $this->promocionesRepository->create($input);

        Flash::success('Promoción guardada correctamente.');

        return redirect(route('promociones.index'));
    }

    /**
     * Display the specified Promociones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        // $promociones = Promociones::with(['localHasProducto'])->find($id);

        $promociones = Promociones::with(['localHasProducto' => function($q) {
            $q->select(DB::raw('CONCAT(producto.nombre, "/", local.nombre) AS nombre'), 'local_has_producto.id')
            ->join('producto', 'producto.id', 'local_has_producto.producto_id')
            ->join('local', 'local.id', 'local_has_producto.local_id');
        }])->find($id);

        if (empty($promociones)) {
            Flash::error('Promoción no encontrada');

            return redirect(route('promociones.index'));
        }

        return view('promociones.show')->with('promociones', $promociones);
    }

    /**
     * Show the form for editing the specified Promociones.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $promociones = Promociones::with(['localHasProducto'])->find($id);
        
        // $LocalHasProducto = LocalHasProducto::with(['local', 'producto'])->get()->pluck('producto.nombre', 'id');

        $LocalHasProducto = LocalHasProducto::select(DB::raw('CONCAT(producto.nombre, "/", local.nombre) AS nombre'), 'local_has_producto.id')
                        ->join('producto', 'producto.id', 'local_has_producto.producto_id')
                        ->join('local', 'local.id', 'local_has_producto.local_id')
                        ->get()
                        ->pluck('nombre', 'id');

        $promociones['producto'] = $LocalHasProducto;

        // dd($promociones);

        if (empty($promociones)) {
            Flash::error('Promoción no encontrada');

            return redirect(route('promociones.index'));
        }

        return view('promociones.edit')->with('promociones', $promociones);
    }

    /**
     * Update the specified Promociones in storage.
     *
     * @param int $id
     * @param UpdatePromocionesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePromocionesRequest $request)
    {
        $promociones = $this->promocionesRepository->find($id);

        if (empty($promociones)) {
            Flash::error('Promoción no encontrada');

            return redirect(route('promociones.index'));
        }

        $promociones = $this->promocionesRepository->update($request->all(), $id);

        Flash::success('Promociones actualizado correctamente.');

        return redirect(route('promociones.index'));
    }

    /**
     * Remove the specified Promociones from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $promociones = $this->promocionesRepository->find($id);

        if (empty($promociones)) {
            Flash::error('Promoción no encontrada');

            return redirect(route('promociones.index'));
        }

        $this->promocionesRepository->delete($id);

        Flash::success('Promociones eliminado correctamente.');

        return redirect(route('promociones.index'));
    }

    public function notificar(Request $request) {

        $input = $request->all();

        $emails = $input['correo_usuarios'];
        
        $promocion = Promociones::with(['localHasProducto'])->find($input['idPromocion']);

        $promocion['invitacion_promocion'] = $input['invitacion_promocion'];

        // dd($promocion->toArray());
        // Envia el correo 
        $this->sendEmail('promociones.emails.template', compact('promocion'), $emails, "Italy Pizza - ¡Súper promoción!");

        Flash::success('Promoción notificada correctamente.');

        return redirect(route('promociones.index'));
    }

    /**
     * Envia correo electronico segun los parametros
     *
     * @author Erika Johana Gonzalez - Mazzo. 04 - 2021
     * @version 1.0.0
     */
    public static function sendEmail($view, $data, $emails, $title){

        if(isset($emails)) {
            Mail::send($view, $data, function ($message) use ( $emails, $title){
                // $message->from(config('mail.username'), $title);
                $message->from('germangv33@gmail.com', $title);
                $message->subject($title);
                $message->to($emails);
            });
        }
    }
}
