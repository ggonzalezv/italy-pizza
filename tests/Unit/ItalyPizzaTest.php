<?php

namespace Tests\Unit;

use Tests\TestCase;

use App\Models\Producto;
use App\Models\User;

class ItalyPizzaTest extends TestCase
{
    /**
     * Validar que al menos haya un usuario en el sistema.
     *
     * @return void
     */
    public function valid_at_least_one_user()
    {
        $cantidad = User::count();
        $this->assertSame(0, $cantidad);
    }

    /** @test */
    public function it_visit_page_of_login()
    {
        $this->get('/login')
            ->assertStatus(200)
            ->assertSee('Italy Pizza');
    }

    /** @test */
    // public function authenticated_to_a_user()
    // {
    //     $this->get('/login')->assertSee('Italy Pizza');

    //     $credentials = [
    //         "email" => "ggonzalezv@uqvirtual.edu.co",
    //         "password" => "12345678"
    //     ];

    //     $response = $this->post('/login', $credentials);
    //     $response->assertRedirect('/home');
    //     $this->assertCredentials($credentials);
    // }

    /** @test */
    public function not_authenticate_to_a_user_with_credentials_invalid()
    {
        $credentials = [
            "email" => "ggonzalezv@uqvirtual.edu.co",
            "password" => "1234"
        ];

        $response = $this->from('/login')->post('/login', $credentials);

        $this->assertInvalidCredentials($credentials);
    }

    /** @test */
    // public function the_email_is_required_for_authenticate()
    // {

    //     $credentials = ['name' => 'London to Paris',
    //         'email' => null,
    //         'password' => '12345678'];

    //     $response = $this->from('/login')->post('/login', $credentials);
    //     $response->assertRedirect('/login')->assertSessionHasErrors([
    //         'email' => 'The email field is required.',
    //     ]);
    // }

    /** @test */
    // public function the_password_is_required_for_authenticate()
    // {
    //     $credentials = [
    //         "email" => "zaratedev@gmail.com",
    //         "password" => null
    //     ];

    //     $response = $this->from('/login')->post('/login', $credentials);
    //     $response->assertRedirect('/login')
    //         ->assertSessionHasErrors([
    //             'password' => 'The password field is required.',
    //         ]);
    // }
}
