<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InicioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();

Route::get('/home', [
    App\Http\Controllers\ProductoController::class, 'index'
])->name('home');

Route::get('/', [
    InicioController::class, 'index'
])->name('inicio');



Route::get('sendEmail', 'App\Http\Controllers\CategoryController@sendEmail');

// Route::get('/', 'App\Http\Controllers\MainController@index');
// Route::post('/send-contact', 'MainController@sendcontact'); 
// Route::get('/interest', 'MainController@interested'); 
// Route::post('/signup', 'MainController@signup'); 
// Route::get('/thank-you', 'MainController@thankyou'); 

Route::resource('productos', App\Http\Controllers\ProductoController::class);

// Route::get('/', 'App\Http\Controllers\ProductoController@inicio');

Route::resource('promociones', App\Http\Controllers\PromocionesController::class);

Route::resource('localHasProductos', App\Http\Controllers\LocalHasProductoController::class);

Route::resource('localHasProductos', App\Http\Controllers\LocalHasProductoController::class);

Route::resource('usuarios', App\Http\Controllers\UsuarioController::class);

Route::post('notificar', 'App\Http\Controllers\PromocionesController@notificar')->name('notificar');

Route::resource('compras', App\Http\Controllers\CompraController::class);

Route::get('get-productos', 'App\Http\Controllers\ProductoController@all')->name('all');

Route::get('get-productos-precio', 'App\Http\Controllers\ProductoController@getProductos');

Route::get('prueba', 'App\Http\Controllers\CompraController@prueba')->name('prueba');

