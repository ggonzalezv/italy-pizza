<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Ciudad
 * @package App\Models
 * @version May 3, 2021, 7:49 pm UTC
 *
 * @property \App\Models\Departamento $departamentodepartamento
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property string $nombre
 * @property integer $departamento_iddepartamento
 */
class Ciudad extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'ciudad';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'departamento_iddepartamento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'departamento_iddepartamento' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'nullable|string|max:45',
        'created_at' => 'nullable|string|max:45',
        'updated_at' => 'nullable|string|max:45',
        'deleted_at' => 'nullable|string|max:45',
        'departamento_iddepartamento' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function departamentodepartamento()
    {
        return $this->belongsTo(\App\Models\Departamento::class, 'departamento_iddepartamento');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'ciudad_idciudad');
    }
}
