<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $promociones->nombre }}</p>
</div>

<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $promociones->descripcion }}</p>
</div>

<!-- Local Has Producto Id Field -->
<div class="col-sm-12">
    {!! Form::label('local_has_producto_id', 'Producto/Local:') !!}
    <p>{{ $promociones->localHasProducto->nombre }}</p>
</div>

