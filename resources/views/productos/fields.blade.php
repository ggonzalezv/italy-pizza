<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 45,'maxlength' => 45, 'required' => true]) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control','maxlength' => 250,'maxlength' => 250, 'required' => true]) !!}
</div>

<!-- Puntos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('puntos', 'Puntos:') !!}
    {!! Form::number('puntos', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Local Field -->
<div class="form-group col-sm-6">
    {!! Form::label('local_id', 'Local:') !!}
    {!! Form::select('local_id', $locales, null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Precio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio', 'Precio ($):') !!}
    {!! Form::number('precio', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Imagen Principal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('imagen_principal', 'Imagen Principal:') !!}
    {!! Form::file('imagen_principal') !!}
</div>