/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 

// importa funcionalidades de toast de manera general
window.toastr = require('toastr');

import './bootstrap';

import Vue from 'vue'

import { Cropper } from 'vue-advanced-cropper';
import vSelect from 'vue-select';
import VueSweetalert2 from 'vue-sweetalert2';
import VCalendar from 'v-calendar';
import Verte from 'verte';
// Libreria para el formato de moneda en los inputs
import VueCurrencyInput from 'vue-currency-input';

import wysiwyg from "vue-wysiwyg";

// Estilos componente select2
import 'vue-select/dist/vue-select.css';
// Estilos componente sweetalert2
import 'sweetalert2/dist/sweetalert2.min.css';

import "vue-wysiwyg/dist/vueWysiwyg.css";

import 'verte/dist/verte.css';

import axios from "axios";

// import DynamicListComponent from './components/DynamicListComponent.vue';


const Lang = require('lang.js');

// Configuracion de archivo de traduccion
let lang = new Lang({
    messages: {
        'es.trans': require('../lang/es.json')
    },
    fallback: 'es'
});

Vue.use(wysiwyg, {});

Vue.use(require('vue-moment'));

Vue.component('dynamic-list', require('./components/DynamicListComponent.vue').default); // Componente de lista dinamica
Vue.component('select-check', require('./components/SelectCheckCrudFieldComponent.vue').default); // 

// Filtro para convertir texto a mayuscula
Vue.filter('uppercase', (value: string) => {
	return value.toUpperCase();
});
// Filtro para convertir la primera letra en mayuscula y el resto en minuscula
Vue.filter('capitalize', (value: string) => {
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
});
// Filtro para la traduccion
Vue.filter('trans', (...value: string[]) => {
    return lang.get(...value);
});


Vue.prototype.$log = console.log;

// "https://github.com/vuejs/vue/issues/214"
Vue.mixin({
	created() {
		const recomputed = Object.create(null)
		const watchers = this._computedWatchers // Warning: Vue internal

		if (!watchers) return

		for (const key in watchers)
			this.makeRecomputable(watchers[key], key, recomputed)
            
		this.$recompute = (key) => recomputed[key] = !recomputed[key]
		Vue.observable(recomputed)
	},
    methods: {
        // Permite recomputarizar manualmente una propiedad computada de un componente por medio del nombre de la propiedad $recompute('Key')
        makeRecomputable(watcher, key, recomputed) {
            const original = watcher.getter
            recomputed[key] = true
            watcher.getter = (vm) => (recomputed[key], original.call(vm, vm))
        }
    }
})


new Vue({
    el: '#app',
    data() {
        return {
            lang: lang,
            dataErrors: {},
            dataForm: {pedido: [], pedido_total: 0}
        };
    },
    methods: {
        submit() {
            this.errors = {};
            axios.post('/compras', this.dataForm).then(response => {
            
            console.log(response.data);
            window.location.href="/compras" ;

            }).catch(error => {
            console.log(error);
            });
        },
        update(id: number) {

            console.log(id);
            // this.errors = {};
            // axios.post('/compras', this.dataForm).then(response => {
            
            //     console.log(response.data);
            //     window.location.href="/compras" ;

            // }).catch(error => {
            //     console.log(error);
            // });
        },
        formatPrice(value) {
            let val = (value/1).toFixed(2).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        },
    },
});
