<?php

namespace App\Repositories;

use App\Models\Promociones;
use App\Repositories\BaseRepository;

/**
 * Class PromocionesRepository
 * @package App\Repositories
 * @version May 6, 2021, 3:31 pm UTC
*/

class PromocionesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'local_has_producto_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Promociones::class;
    }
}
